/**
 * Module dependencies.
 */

application = (function () {
	var express = require('express'),
		bodyParser = require("body-parser"),
		route = require('./app/route');
    
	var app = global.app = module.exports = express(),
    	server = require('http').createServer(app);
    
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(express.static(__dirname + '/public'));
    server.listen(8222);
    
    route.init(app);
}).call(this);
