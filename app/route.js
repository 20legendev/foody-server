var FoodController = require('./controller/FoodController');

Route = {
	init : function(app) {
		this.app = app;
		this.handle();
	},

	handle : function() {
		this.app.get('/api/food', FoodController.listFood);
	}
}
module.exports = Route;