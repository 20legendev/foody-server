var couchbase = require('couchbase');

Database = {
	init : function() {
		var cluster = new couchbase.Cluster('couchbase://localhost');
		this.bucket = cluster.openBucket('foody');
	},
	getSession : function() {
		return this.bucket;
	}
}

module.exports = Database;